package ru.t1.sochilenkov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void writeLog(@NotNull final String message);

}
