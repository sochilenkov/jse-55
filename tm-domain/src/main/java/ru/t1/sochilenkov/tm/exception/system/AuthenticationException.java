package ru.t1.sochilenkov.tm.exception.system;

public class AuthenticationException extends AbstractSystemException {

    public AuthenticationException() {
        super("Authentication failed...");
    }

}
